(* This program proves that IL + G' = Gödel–Dummett Logic (IL + G) *)


Definition G := 
  forall a b c: Prop, ((a -> b) -> c) -> ((b -> a) -> c) -> c.

Definition G' :=
  forall a a' b b' c c' c'' c''': Prop,
  ((a -> a') -> (b' -> b) -> (c'' -> c') -> (c''' -> c') ->
  ((a -> b) -> c'') -> ((b' -> a') -> c''') ->
  (((a' -> b') -> c'') -> ((b -> a) -> c''') -> c' -> c) -> c).

Proposition G_iff_G':
  G <-> G'.
Proof.
  split; [(intro G; unfold G') | (intro G'; unfold G)]; intros *.
  - specialize (G a b c). tauto.
  - specialize (G' a a b b c c c c). tauto.
Qed.

Proposition G_iff_G'_another_proof:
  G <-> G'.
Proof.
  split; [(intro G; unfold G') | (intro G'; unfold G)]; intros *.
  - specialize (G a b c).
    intros H1 H2 H3 H4 H5 H6 H7.
    apply G.
    -- intros H8. apply H7.
      --- intros _. apply H5. exact H8.
      --- intros H9. apply H6. intros H10. apply H1. apply H9. apply H2. exact H10.
      --- apply H3. apply H5. exact H8.
    -- intros H8. apply H7.
      --- intros H9. apply H5. intros H10. apply H2. apply H9. apply H1. exact H10.
      --- intros _. apply H6. intros H9. apply H1. apply H8. apply H2. exact H9.
      --- apply H4. apply H6. intros H9. apply H1. apply H8. apply H2. exact H9.
  - specialize (G' a a b b c c c c).
    intros H1 H2.
    apply G'.
    -- intros H3. exact H3.
    -- intros H3. exact H3.
    -- intros H3. exact H3.
    -- intros H3. exact H3.
    -- exact H1.
    -- exact H2.
    -- intros _ _ H3. apply H3.
Qed.