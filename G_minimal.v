(* This program checks that G' is minimal in CL *)
Require Import Classical.
Variables a a' b b' c c' c'' c''': Prop.
Variable X: Prop.

Notation G' := ((a -> a') -> (b' -> b) -> (c'' -> c') -> (c''' -> c') ->
((a -> b) -> c'') -> ((b' -> a') -> c''') ->
(((a' -> b') -> c'') -> ((b -> a) -> c''') -> c' -> c) -> c).
Proposition check: G'.
Proof.
intros.
tauto.
Qed.

Notation G'_11 := ((X -> a') -> (b' -> b) -> (c'' -> c') -> (c''' -> c') ->
((a -> b) -> c'') -> ((b' -> a') -> c''') ->
(((a' -> b') -> c'') -> ((b -> a) -> c''') -> c' -> c) -> c).
Proposition check_11: G'_11.
Proof.
intros.
Fail tauto.
Admitted.

Notation G'_12 := ((a -> a') -> (b' -> b) -> (c'' -> c') -> (c''' -> c') ->
((X -> b) -> c'') -> ((b' -> a') -> c''') ->
(((a' -> b') -> c'') -> ((b -> a) -> c''') -> c' -> c) -> c).
Proposition check_12: G'_12.
Proof.
intros.
Fail tauto.
Admitted.

Notation G'_13 := ((a -> a') -> (b' -> b) -> (c'' -> c') -> (c''' -> c') ->
((a -> b) -> c'') -> ((b' -> a') -> c''') ->
(((a' -> b') -> c'') -> ((b -> X) -> c''') -> c' -> c) -> c).
Proposition check_13: G'_13.
Proof.
intros.
Fail tauto.
Admitted.

Notation G'_21 := ((a -> a') -> (b' -> X) -> (c'' -> c') -> (c''' -> c') ->
((a -> b) -> c'') -> ((b' -> a') -> c''') ->
(((a' -> b') -> c'') -> ((b -> a) -> c''') -> c' -> c) -> c).
Proposition check_21: G'_21.
Proof.
intros.
Fail tauto.
Admitted.

Notation G'_22 := ((a -> a') -> (b' -> b) -> (c'' -> c') -> (c''' -> c') ->
((a -> X) -> c'') -> ((b' -> a') -> c''') ->
(((a' -> b') -> c'') -> ((b -> a) -> c''') -> c' -> c) -> c).
Proposition check_22: G'_22.
Proof.
intros.
Fail tauto.
Admitted.

Notation G'_23 := ((a -> a') -> (b' -> b) -> (c'' -> c') -> (c''' -> c') ->
((a -> b) -> c'') -> ((b' -> a') -> c''') ->
(((a' -> b') -> c'') -> ((X -> a) -> c''') -> c' -> c) -> c).
Proposition check_23: G'_23.
Proof.
intros.
Fail tauto.
Admitted.

Notation G'_31 := ((a -> X) -> (b' -> b) -> (c'' -> c') -> (c''' -> c') ->
((a -> b) -> c'') -> ((b' -> a') -> c''') ->
(((a' -> b') -> c'') -> ((b -> a) -> c''') -> c' -> c) -> c).
Proposition check_31: G'_31.
Proof.
intros.
Fail tauto.
Admitted.

Notation G'_32 := ((a -> a') -> (b' -> b) -> (c'' -> c') -> (c''' -> c') ->
((a -> b) -> c'') -> ((b' -> X) -> c''') ->
(((a' -> b') -> c'') -> ((b -> a) -> c''') -> c' -> c) -> c).
Proposition check_32: G'_32.
Proof.
intros.
Fail tauto.
Admitted.

Notation G'_33 := ((a -> a') -> (b' -> b) -> (c'' -> c') -> (c''' -> c') ->
((a -> b) -> c'') -> ((b' -> a') -> c''') ->
(((X -> b') -> c'') -> ((b -> a) -> c''') -> c' -> c) -> c).
Proposition check_33: G'_33.
Proof.
intros.
Fail tauto.
Admitted.

Notation G'_41 :=((a -> a') -> (X -> b) -> (c'' -> c') -> (c''' -> c') ->
((a -> b) -> c'') -> ((b' -> a') -> c''') ->
(((a' -> b') -> c'') -> ((b -> a) -> c''') -> c' -> c) -> c).
Proposition check_41: G'_41.
Proof.
intros.
Fail tauto.
Admitted.

Notation G'_42 := ((a -> a') -> (b' -> b) -> (c'' -> c') -> (c''' -> c') ->
((a -> b) -> c'') -> ((X -> a') -> c''') ->
(((a' -> b') -> c'') -> ((b -> a) -> c''') -> c' -> c) -> c).
Proposition check_42: G'_42.
Proof.
intros.
Fail tauto.
Admitted.

Notation G'_43 := ((a -> a') -> (b' -> b) -> (c'' -> c') -> (c''' -> c') ->
((a -> b) -> c'') -> ((b' -> a') -> c''') ->
(((a' -> X) -> c'') -> ((b -> a) -> c''') -> c' -> c) -> c).
Proposition check_43: G'_43.
Proof.
intros.
Fail tauto.
Admitted.

Notation G'_51 := ((a -> a') -> (b' -> b) -> (c'' -> c') -> (c''' -> c') ->
((a -> b) -> c'') -> ((b' -> a') -> c''') ->
(((a' -> b') -> c'') -> ((b -> a) -> c''') -> c' -> X) -> c).
Proposition check_51: G'_51.
Proof.
intros.
Fail tauto.
Admitted.

Notation G'_61 := ((a -> a') -> (b' -> b) -> (c'' -> X) -> (c''' -> c') ->
((a -> b) -> c'') -> ((b' -> a') -> c''') ->
(((a' -> b') -> c'') -> ((b -> a) -> c''') -> c' -> c) -> c).
Proposition check_61: G'_61.
Proof.
intros.
Fail tauto.
Admitted.

Notation G'_62 := ((a -> a') -> (b' -> b) -> (c'' -> c') -> (c''' -> X) ->
((a -> b) -> c'') -> ((b' -> a') -> c''') ->
(((a' -> b') -> c'') -> ((b -> a) -> c''') -> c' -> c) -> c).
Proposition check_62: G'_62.
Proof.
intros.
Fail tauto.
Admitted.

Notation G'_63 := ((a -> a') -> (b' -> b) -> (c'' -> c') -> (c''' -> c') ->
((a -> b) -> c'') -> ((b' -> a') -> c''') ->
(((a' -> b') -> c'') -> ((b -> a) -> c''') -> X -> c) -> c).
Proposition check_63: G'_63.
Proof.
intros.
Fail tauto.
Admitted.


Notation G'_71 := ((a -> a') -> (b' -> b) -> (X -> c') -> (c''' -> c') ->
((a -> b) -> c'') -> ((b' -> a') -> c''') ->
(((a' -> b') -> c'') -> ((b -> a) -> c''') -> c' -> c) -> c).
Proposition check_71: G'_71.
Proof.
intros.
Fail tauto.
Admitted.

Notation G'_72 := ((a -> a') -> (b' -> b) -> (c'' -> c') -> (c''' -> c') ->
((a -> b) -> X) -> ((b' -> a') -> c''') ->
(((a' -> b') -> c'') -> ((b -> a) -> c''') -> c' -> c) -> c).
Proposition check_72: G'_72.
Proof.
intros.
Fail tauto.
Admitted.

Notation G'_73 := ((a -> a') -> (b' -> b) -> (c'' -> c') -> (c''' -> c') ->
((a -> b) -> c'') -> ((b' -> a') -> c''') ->
(((a' -> b') -> X) -> ((b -> a) -> c''') -> c' -> c) -> c).
Proposition check_73: G'_73.
Proof.
intros.
Fail tauto.
Admitted.

Notation G'_81 := ((a -> a') -> (b' -> b) -> (c'' -> c') -> (X -> c') ->
((a -> b) -> c'') -> ((b' -> a') -> c''') ->
(((a' -> b') -> c'') -> ((b -> a) -> c''') -> c' -> c) -> c).
Proposition check_81: G'_81.
Proof.
intros.
Fail tauto.
Admitted.

Notation G'_82 := ((a -> a') -> (b' -> b) -> (c'' -> c') -> (c''' -> c') ->
((a -> b) -> c'') -> ((b' -> a') -> X) ->
(((a' -> b') -> c'') -> ((b -> a) -> c''') -> c' -> c) -> c).
Proposition check_82: G'_82.
Proof.
intros.
Fail tauto.
Admitted.

Notation G'_83 := ((a -> a') -> (b' -> b) -> (c'' -> c') -> (c''' -> c') ->
((a -> b) -> c'') -> ((b' -> a') -> c''') ->
(((a' -> b') -> c'') -> ((b -> a) -> X) -> c' -> c) -> c).
Proposition check_83: G'_83.
Proof.
intros.
Fail tauto.
Admitted.
