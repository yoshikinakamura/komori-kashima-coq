(* This program proves that IL + J' = IL + J'' = Jankov-De Morgan logic (IL + weak_excluded_middle) *)
Require Import ClassicalFacts.

Definition J' :=
  forall a b c:Prop, (a -> b) \/ ((a -> False) -> c).


Proposition weak_excluded_middle_iff_J' :
  weak_excluded_middle <-> J'.
Proof.
  split; [intro WEM|intro J']; intros a *.
  - destruct (WEM a); tauto.
  - destruct (J' a False False); tauto.
Qed.


Definition J'' :=
  forall a b c d:Prop, ((a -> b) -> d) -> (((a -> False) -> c) -> d) -> d.


Proposition weak_excluded_middle_iff_J'' :
  weak_excluded_middle <-> J''.
Proof.
Proof.
  split; [intro WEM|intro J'']; intros a *.
  - destruct (WEM a); tauto.
  - destruct (J'' a False False (((a -> False)) \/ ((a -> False) -> False))); tauto.
Qed.
