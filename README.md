# Coq Files for “On the Axiomatization of Implicational Intermediate Logics with Formulas Minimal in Classical Logic: A Counter-Example to the Komori-Kashima Problem"

This project consists of supplement coq files for the paper
“On the Axiomatization of Implicational Intermediate Logics with Formulas Minimal in Classical Logic: A Counter-Example to the Komori-Kashima Problem".

## Requirements

- [Coq 8.11.0](https://github.com/coq/coq/releases/tag/V8.11.0)

## Author

[Yoshiki Nakamura](mailto: nakamura.yoshiki.ny@gmail.com)
